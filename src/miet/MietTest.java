package miet;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


public class MietTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		MietParser mietParser = new MietParser(args[0], Integer.parseInt(args[1]));
		
		try {
			//mietParser.SaveHTML(mietParser.URL, "url1.html");

			for (int i=0; i < 1000000000; i++)
			{
		        mietParser.RecieveAll();	        
		        mietParser.Parse(MietParser.MPITK);
		        mietParser.Parse(MietParser.EKT);
		        mietParser.Parse(MietParser.ITS);
	
		        Report rMPITK = mietParser.Get(MietParser.MPITK);
		        Report rEKT = mietParser.Get(MietParser.EKT);
		        Report rITS = mietParser.Get(MietParser.ITS);

				DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm");

				// Get the date today using Calendar object.
				Date today = Calendar.getInstance().getTime();        
				// Using DateFormat format method we can create a string 
				// representation of a date with the defined format.
				String reportDate = df.format(today);
		        
		        System.out.printf("%s\n", reportDate); 
		        System.out.printf("МПиТК место %3d из %3d, общежитие %3d из %3d - оригиналов %3d\n", rMPITK.place, rMPITK.totalPlace, rMPITK.placeCampus, rMPITK.totalPlaceCampus, rMPITK.vacancytPlace);
		        System.out.printf("  ЭКТ место %3d из %3d, общежитие %3d из %3d - оригиналов %3d\n", rEKT.place, rEKT.totalPlace, rEKT.placeCampus, rEKT.totalPlaceCampus, rEKT.vacancytPlace);
		        System.out.printf("  ИТС место %3d из %3d, общежитие %3d из %3d - оригиналов %3d\n\n", rITS.place, rITS.totalPlace, rITS.placeCampus, rITS.totalPlaceCampus, rITS.vacancytPlace);
		        
		        System.out.printf("Выдержим:\n"); 
		        
		        if (rMPITK.totalPlace-rMPITK.place >= 0)
		        	System.out.printf(" МПиТК %d/%d", rMPITK.totalPlace-rMPITK.place, rMPITK.totalPlaceCampus-rMPITK.placeCampus);
		        if (rEKT.totalPlace-rEKT.place >= 0)
		        	System.out.printf(" ЭКТ %d/%d", rEKT.totalPlace-rEKT.place, rEKT.totalPlaceCampus-rEKT.placeCampus);
		        if (rITS.totalPlace-rITS.place >= 0)
		        	System.out.printf(" ИТС %d/%d", rITS.totalPlace-rITS.place, rITS.totalPlaceCampus-rITS.placeCampus);
		        System.out.printf("\n\n");

		        Thread.sleep(1000*60*5);
			}	

			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
