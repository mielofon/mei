package miet;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.*;
import java.io.*;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
//import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

class URLRec {
	public String url;
	public String urlCampus;
	public String fileName;
	
	public String htmlPlace;
	public String htmlPlaceCampus;
	
	public Report report; 
	
	public URLRec(String url, String urlCampus, String fileName) {
		this.url = url;
		this.urlCampus = urlCampus;
		this.fileName = fileName;

		this.htmlPlace = null;
		this.htmlPlaceCampus = null;

		report = new Report(); 
	}
}

public class MietParser {
	static final public String LOG_DIR = "e:\\workspace\\MIET\\log\\"; 
	static final public String LOG_EXT = ".html";
	
	
	static final public int MPITK = 0;
	static final public int EKT = 1;
	static final public int ITS = 2;
	private static final String CAMPUS_POSTFIX = "_campus";
	
	public List<URLRec> urlList;
	public String regNumber = "";
	int points = 0;
	
	public MietParser(String aRegNumber, int aPoints)
	{
		regNumber = aRegNumber;
		points = aPoints;
		
		urlList = new ArrayList<URLRec>(Arrays.asList(
				//new URLRec("http://abite.miet.ru/app-origins/listteh.pl?dep=4", "MPITK_EKT_ITS"),
				new URLRec("http://abite.miet.ru/app-origins/list.pl?dep=1", 
						"http://abite.miet.ru/app-origins/listcampus.pl?dep=12",
						"MPITK"),
				new URLRec("http://abite.miet.ru/app-origins/list.pl?dep=2", 
						"http://abite.miet.ru/app-origins/listcampus.pl?dep=13",
						"EKT"),
				new URLRec("http://abite.miet.ru/app-origins/list.pl?dep=3",
						"http://abite.miet.ru/app-origins/listcampus.pl?dep=14",
						"ITS")//,
				//new URLRec("http://abite.miet.ru/app-origins/listcampus.pl?dep=12", "MPITK_campus"),
				//new URLRec("http://abite.miet.ru/app-origins/listcampus.pl?dep=13", "EKT_campus"),
				//new URLRec("http://abite.miet.ru/app-origins/listcampus.pl?dep=14", "ITS_campus"),
				//new URLRec("http://abite.miet.ru/recommended/list.pl?dep=1", "MPITK_rekomend"),
				//new URLRec("http://abite.miet.ru/recommended/list.pl?dep=2", "EKT_rekomend"),
				//new URLRec("http://abite.miet.ru/recommended/list.pl?dep=3", "ITS_rekomend")
				));
		
	}

	public MietParser()
	{
		this("", 0);
	}
	
	public Report ParsePlace(int index)
	{
		
		URLRec rec = urlList.get(index);
		Report report = rec.report;
		
		if ( rec.htmlPlace == null)
		{
			RecieveHTML(index);
		}		
		
		Document doc = Jsoup.parse(rec.htmlPlace);
        
		Elements tables = doc.select("table");
		Element table = tables.get(0);
		Elements tr = table.select("tr");
		
		String[] arr = tr.get(3).select("td").get(0).text().split(" ");
		report.dayToEnd = Integer.parseInt(arr[6]); //t[0][4] -> �� ����� ����� ���������� ���������� �������� 3 ���
		arr = tr.get(6).select("td").get(0).text().split(" ");
		report.totalPlace = Integer.parseInt(arr[3]); //Число вакантных мест: 168
		arr = tr.get(7).select("td").get(0).text().split(" ");
		report.vacancytPlace = Integer.parseInt(arr[2]); //Число претендентов: 196 
		
		boolean flagSetPlace = false;
		
		for (int i = 9; i < tr.size()-1; i++)
		{
			Elements tds = tr.get(i).select("td");
			RowReport row =  new RowReport();
			
			try 
			{
				row.index = Integer.parseInt(tds.get(0).text());
			} 
			catch (Exception e) 
			{
				print("Error!! "+ tds.get(0).text());
			}
			row.regNumber = tds.get(1).text();
			row.name = tds.get(2).text();
			row.value = Integer.parseInt(tds.get(3).text());
			
			if (!flagSetPlace)
			{
				if (row.regNumber.equals(regNumber) || row.value < points)
				{
					report.place = row.index; 
					flagSetPlace = true;
				}
				else if (i == tr.size()-2)
				{
					report.place = row.index+1; 
					flagSetPlace = true;
				}
			}
			
			report.report.add(row);
		}
				
		return report;
	}
	
	public Report ParsePlaceCampus(int index)
	{
		
		URLRec rec = urlList.get(index);
		Report report = rec.report;
		
		if ( rec.htmlPlaceCampus == null)
		{
			RecieveHTML(index);
		}

		
		
		Document doc = Jsoup.parse(rec.htmlPlaceCampus);
        
		Elements tables = doc.select("table");
		Element table = tables.get(0);
		Elements tr = table.select("tr");
		
		String[] arr = tr.get(3).select("td").get(0).text().split(" ");
//		report.dayToEnd = Integer.parseInt(arr[6]); //t[0][4] -> �� ����� ����� ���������� ���������� �������� 3 ���
		arr = tr.get(6).select("td").get(0).text().split(" ");
		report.totalPlaceCampus = Integer.parseInt(arr[3]); //Число вакантных мест: 168
		arr = tr.get(7).select("td").get(0).text().split(" ");
		report.vacancyCampus = Integer.parseInt(arr[4]); //Число претендентов: 196 
		
		boolean flagSetPlace = false;
		
		for (int i = 9; i < tr.size()-1; i++)
		{
			Elements tds = tr.get(i).select("td");
			RowReport row =  new RowReport();
			
			try 
			{
				row.index = Integer.parseInt(tds.get(0).text());
			} 
			catch (Exception e) 
			{
				print("Error!! "+ tds.get(0).text());
			}
			row.regNumber = tds.get(1).text();
			row.name = tds.get(2).text();
			row.value = Integer.parseInt(tds.get(3).text());
			
			if (!flagSetPlace)
			{
				if (row.regNumber.equals(regNumber) || row.value < points)
				{
					report.placeCampus = row.index; 
					flagSetPlace = true;
				}
				else if (i == tr.size()-2)
				{
					report.placeCampus = row.index+1; 
					flagSetPlace = true;
				}
			}
			
			report.reportCampus.add(row);
		}
				
		return report;
	}
	
    private static void print(String msg, Object... args) {
        System.out.println(String.format(msg, args));
    }

    private static String trim(String s, int width) {
        if (s.length() > width)
        {
            return s.substring(0, width-1) + ".";
        }
        else
        {
            return s;
        }
    }
    
    private String GetDataPostfix()
    {
		DateFormat df = new SimpleDateFormat("MMddyyyyHHmmss");

		// Get the date today using Calendar object.
		Date today = Calendar.getInstance().getTime();        
		// Using DateFormat format method we can create a string 
		// representation of a date with the defined format.
		String reportDate = df.format(today);
		return reportDate;
    }
    

    public void SaveHTML(int index)
    {
    	SaveHTML(index, false);
    }
    
    public void SaveHTML(int index, boolean dataPostfix)
    {
    	URLRec report = urlList.get(index);
    	String fileName = report.fileName;
    	
    	SaveHTML(report.htmlPlace, fileName, dataPostfix);

    	fileName = report.fileName + CAMPUS_POSTFIX;
    	SaveHTML(report.htmlPlaceCampus, fileName, dataPostfix);    	
    }

    public void RecieveHTML(int index)
    {
    	URLRec report = urlList.get(index);
    	String url = report.url;
    	
    	report.htmlPlace = "";
    	try {
			report.htmlPlace = RecieveHTML(url);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    	url = report.urlCampus;
    	
    	report.htmlPlaceCampus = "";
    	try {
			report.htmlPlaceCampus = RecieveHTML(url);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public void SaveHTML(String html, String fileName, boolean dataPostfix)
    {
    	if ( dataPostfix)
    	{
    		fileName = LOG_DIR + fileName + "." + GetDataPostfix() + LOG_EXT;  
    	}
    	else
    	{
    		fileName = LOG_DIR + fileName + LOG_EXT;
    	}
    	 

    	try {
            //print("Fetching %s...", url);

            PrintWriter fout = new PrintWriter   // класс с методами записи строк
            		  (new OutputStreamWriter          // класс-преобразователь
            		     (new FileOutputStream         // класс записи байтов в файл
            		        (fileName), "windows-1251"));
            
            
        	fout.print(html);

            fout.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    }


    public String RecieveHTML(String url) throws ClientProtocolException, IOException
    {
        HttpClient httpClient = new DefaultHttpClient();
        HttpContext localContext = new BasicHttpContext();
        HttpGet httpGet = new HttpGet(url);
        HttpResponse response = httpClient.execute(httpGet, localContext);
        String result = "";

        BufferedReader reader = new BufferedReader(
            new InputStreamReader(
              response.getEntity().getContent()
            )
          );

        String line = null;
        while ((line = reader.readLine()) != null){
          result += line + "\n";
//          Toast.makeText(activity.this, line.toString(), Toast.LENGTH_LONG).show();
        }
        return result;

    }
    
    
    public String RecieveHTML_2(String url)
    {
		try {
			URL oracle = new URL(url);
	
			try
			{
				oracle.openConnection();
				
				InputStream stream = oracle.openStream();
				InputStreamReader bfr = new InputStreamReader(stream, "windows-1251");
				BufferedReader in = new BufferedReader(bfr);
		
		
				String inputLine;
		        String html = "";
		        try {
					while ((inputLine = in.readLine()) != null)
					{
						html += inputLine+'\n';
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					html = "";
				}
		        in.close();
		        
		        return html;
		        
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		/*
		HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(httpPath);
        try {

            HttpEntity httpEntity = httpclient.execute(httpget).getEntity();

           if (httpEntity != null){
            InputStream inputStream = httpEntity.getContent();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }

            inputStream.close();

            tvHttp.setText(stringBuilder.toString());
           }

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_LONG).show();
      }		
		*/
		
		return "";        
	}

    	
    public String RecieveHTML_(String url)
    {
		try {
			URL oracle = new URL(url);

			BufferedReader in;
			try {
				
				
				in = new BufferedReader(
						new InputStreamReader(oracle.openStream(), "windows-1251"));
		        
				String inputLine;
		        String html = "";
		        try {
					while ((inputLine = in.readLine()) != null)
					{
						html += inputLine+'\n';
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					html = "";
				}
		        in.close();
		        
		        return html;
		        
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";        
    }

    public String LoadHTML(int index)
    {
    	return LoadHTML(index, "");
    }
    
    public String LoadHTML(int index, String dataStr)
    {
    	String fileName = urlList.get(index).fileName;
    	if ( !dataStr.equals(""))
    	{
    		fileName = LOG_DIR + fileName + dataStr + LOG_EXT;  
    	}
    	else
    	{
    		fileName = LOG_DIR + fileName + LOG_EXT;
    	}

		try {
			FileInputStream fin = new FileInputStream(fileName);
	    	byte[] data = new byte[fin.available()];    
	    	fin.read(data);  
	    	String htmlFile = new String(data);
	    	return htmlFile;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
	    	return "";
		} catch (IOException e) {
			// TODO Auto-generated catch block
	    	return "";
		}     
    	
    }

	public void SaveAll() {
		for( int i = 0; i < urlList.size(); i++) {
			RecieveHTML(i);	
			SaveHTML(i, true);	
		}		
	}

	public void RecieveAll() {
		for( int i = 0; i < urlList.size(); i++) {
			RecieveHTML(i);	
		}		
	}

	public void Parse(int index) {
		ParsePlace(index);
		ParsePlaceCampus(index);		
	}

	public Report Get(int index) {
		// TODO Auto-generated method stub
		return urlList.get(index).report;
	}
}
